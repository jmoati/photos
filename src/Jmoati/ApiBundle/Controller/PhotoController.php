<?php

namespace Jmoati\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Jmoati\PhotosBundle\Entity\Photo;

class PhotoController extends FOSRestController
{
    /**
     * @Method({"GET"})
     * @Route("photo/list", name="api_photo_list")
     */
    public function listAction(Request $request)
    {
        $results = $this->get('app.indexation')->find();

        $items = array();
        foreach($results as $result) {
            $items[] = $result->getSource();
        }

        $view = $this
            ->view($items, 200);

        return $this->handleView($view);
    }

    /**
     * @Method({"GET"})
     * @Route("photo/hashs", name="api_photo_hashs")
     */
    public function hashsAction()
    {
        return new JsonResponse($this->get('app.photos')->getHashs());
    }

    /**
     * @Method({"POST"})
     * @Route("photo/upload", name="api_photo_upload")
     */
    public function uploadAction(Request $request)
    {
        $entity = $this->get('app.photos')->handleRequest($request);

        return new JsonResponse(array('success' => $entity instanceof Photo));
    }

}
