var photonizer = angular.module('photonizer', ['ngRoute', 'lrInfiniteScroll', 'photonizerControllers']);

photonizer.config(['$routeProvider',
        function($routeProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: Routing.generate('partial_index'),
                    controller: 'indexCtrl'})
                .when('/login', {
                    templateUrl: Routing.generate('partial_login'),
                    controller: 'loginCtrl'})
                .otherwise({redirectTo: '/'})
        }]
);

photonizer.directive('onLastRepeat', function() {
    return function(scope, element, attrs) {
        if (scope.$last) setTimeout(function(){
            $("#photos").justifiedGallery({
                rowHeight: 250,
                cssAnimation: true
            });
        }, 1);
    };
});

photonizer.run(function($http, $window){
        if (window.sessionStorage.token !== null){
            $http.defaults.headers.common.Authorization = 'Bearer ' + $window.sessionStorage.token;
        }
    }
);

var photonizerControllers = angular.module('photonizerControllers', []);

photonizerControllers
    .controller('indexCtrl', ['$scope', '$http', '$window', '$location', function($scope, $http, $window, $location){
        if (null == window.sessionStorage.token){
            $location.path('/login').replace();
            return;
        }

        $http
            .get(Routing.generate('api_photo_list'), [])
            .success(function (data, status, headers, config) {
                $scope.photos = data;
            })
            .error(function (data, status, headers, config) {
                console.log('error');
            });

}]);

photonizerControllers
    .controller('loginCtrl', ['$scope', '$http', '$window', '$location', function($scope, $http, $window, $location){
        $scope.message = '';
        $scope.submit = function (){
            $http
                .post(Routing.generate('api_login_check'), $scope.user)
                .success(function (data, status, headers, config) {
                    $window.sessionStorage.token = data.token;
                    $http.defaults.headers.common.Authorization = 'Bearer ' + $window.sessionStorage.token;

                    $location.path('/').replace();
                })
                .error(function (data, status, headers, config) {
                    $scope.message = Translator.trans('login.error');
                });
        };
    }]);

addMoreItems = function(){
    console.log('test');
}