<?php

namespace Jmoati\PhotosBundle\Processor;

use Doctrine\ORM\EntityManager;
use Jmoati\PhotosBundle\Service\PhotoService;
use Swarrot\Broker\Message;
use Swarrot\Processor\ProcessorInterface;

class ThumbProcessor implements ProcessorInterface
{
    protected $entityManager;
    protected $photoService;

    public function __construct(EntityManager $entityManager, PhotoService $photoService)
    {
        $this->entityManager = $entityManager;
        $this->photoService = $photoService;
    }

    public function process(Message $message, array $options)
    {
        $id = $message->getBody();

        $photo = $this
            ->entityManager
            ->getRepository('JmoatiPhotosBundle:Photo')
            ->find($id);
        ;

        $this->photoService->createThumb($photo);
    }
}