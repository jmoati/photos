<?php

namespace Jmoati\PhotosBundle\Controller;

use Elastica\JSON;
use Jmoati\PhotosBundle\Entity\Photo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PartialsController extends Controller
{
    /**
     * @Method({"GET"})
     * @Route("/partials/index", name="partial_index")
     */
    public function indexAction()
    {
        return $this->render('JmoatiPhotosBundle:Partials:index.html.twig', array());
    }

    /**
     * @Method({"GET"})
     * @Route("/partials/login", name="partial_login")
     */
    public function loginAction()
    {
        return $this->render('JmoatiPhotosBundle:Partials:login.html.twig', array());
    }
}
