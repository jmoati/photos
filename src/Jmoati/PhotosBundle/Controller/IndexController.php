<?php

namespace Jmoati\PhotosBundle\Controller;

use Elastica\JSON;
use Jmoati\PhotosBundle\Entity\Photo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends Controller
{
    /**
     * @Method({"GET"})
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('JmoatiPhotosBundle:Index:index.html.twig', array());
    }
}
