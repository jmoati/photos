<?php

namespace Jmoati\PhotosBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Jmoati\HelperBundle\Traits\Entity;
use Jmoati\HelperBundle\Traits\Timestampable;
use stdClass;
use DateTime;


/**
 * @ORM\Table(name="photo")
 * @ORM\Entity()
 */
class Photo
{
    use Entity;
    use Timestampable;

    /**
     * @ORM\Column(type="string", length=13)
     */
    protected $reference;

    /**
     * @ORM\Column(type="string")
     */
    protected $mimetype;

    /**
     * @ORM\Column(type="string", length=64)
     */
    protected $hash;

    /**
     * @ORM\Column(type="json_array")
     */
    protected $exif;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $date;

    /**
     * @ORM\Column(type="integer")
     */
    protected $filesize;

    /**
     * @ORM\Column(type="text")
     */
    protected $originalFilePath;

    /**
     * @param string $reference
     * @return Photo
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }


    /**
     * @param string $hash
     * @return Photo
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }


    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }


    /**
     * @param integer $filesize
     * @return Photo
     */
    public function setFilesize($filesize)
    {
        $this->filesize = $filesize;

        return $this;
    }


    /**
     * @return integer
     */
    public function getFilesize()
    {
        return $this->filesize;
    }


    /**
     * @param string $originalFilePath
     * @return Photo
     */
    public function setOriginalFilePath($originalFilePath)
    {
        $this->originalFilePath = $originalFilePath;

        return $this;
    }


    /**
     * @return string
     */
    public function getOriginalFilePath()
    {
        return $this->originalFilePath;
    }


    /**
     * @param stdClass $exif
     * @return Photo
     */
    public function setExif($exif)
    {
        $this->exif = $exif;

        return $this;
    }


    /**
     * @return stdClass
     */
    public function getExif()
    {
        return $this->exif;
    }


    /**
     * @param DateTime $date
     * @return Photo
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }


    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }


    /**
     * @param string  $mimetype
     * @return Photo
     */
    public function setMimetype($mimetype)
    {
        $this->mimetype = $mimetype;
    
        return $this;
    }
    

    /**
     * @return string 
     */
    public function getMimetype()
    {
        return $this->mimetype;
    }
    
}