<?php

namespace Jmoati\PhotosBundle\Service;

use Doctrine\Common\Persistence\ManagerRegistry;
use Jmoati\ExifToolBundle\Service\ExifToolService;
use Jmoati\FFMpeg\Data\Output;
use Jmoati\FFMpeg\Data\Timecode;
use Jmoati\FFMpeg\FFMpeg;
use Jmoati\PhotosBundle\Entity\Hash;
use Jmoati\PhotosBundle\Entity\Photo;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class PhotoService
{
    /**
     * @var string
     */
    protected $appDir;

    /**
     * @var ManagerRegistry
     */
    protected $managerRegistry;

    /**
     * @var Filesystem
     */
    protected $filesystemService;

    /**
     * @var IndexationService
     */
    protected $indexationService;

    /**
     * @var ExifToolService
     */
    protected $exifToolService;

    /**
     * @param string $appDir
     * @param ManagerRegistry $managerRegistry
     * @param Filesystem $filesystemService
     * @param IndexationService $indexationService
     * @param ExifToolService $exifToolService
     */
    public function __construct($appDir, ManagerRegistry $managerRegistry, Filesystem $filesystemService, IndexationService $indexationService, ExifToolService $exifToolService)
    {
        $this->appDir = $appDir;
        $this->managerRegistry = $managerRegistry;
        $this->filesystemService = $filesystemService;
        $this->indexationService = $indexationService;
        $this->exifToolService = $exifToolService;

        $this->indexationService->setPhotoService($this);
    }

    /**
     * @param Request $request
     *
     * @return boolean|Photo
     */
    public function handleRequest(Request $request)
    {
        if (Request::METHOD_POST !== $request->getMethod()) {
            return false;
        }

        return $this->handleUpload(current($request->files->get('files')), $request);
    }

    /**
     * @param UploadedFile $upload
     * @return Photo
     */
    protected function handleUpload(UploadedFile $upload)
    {
        return $this->create(
            $upload->getRealPath(),
            $upload->getClientOriginalName()
        );
    }

    /**
     * @param string $filepath
     *
     * @return string
     */
    public function getMimeType($filepath)
    {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimetype = finfo_file($finfo, $filepath);
        finfo_close($finfo);

        return $mimetype;
    }

    /**
     * @param string $filepath
     *
     * @return string
     */
    public function getHash($filepath)
    {
        return hash_file('sha256', $filepath);
    }

    /**
     * @param string $path
     * @param string $originalFilepath
     *
     * @return Photo
     */
    public function create($path, $originalFilepath)
    {
        $hash = $this->getHash($path);
        $filesize = filesize($path);
        $mimetype = $this->getMimeType($path);

        if (strstr($mimetype, 'application') || $this->hashHash($hash)) {
            return false;
        }

        $entity = Photo::create()
            ->setReference(uniqid())
            ->addHash(Hash::create()
                ->setFilesize($filesize)
                ->setHash($hash)
                ->setOriginalFilePath($originalFilepath));

        $this->filesystemService->copy(
            $path,
            $this->getFilePath($entity)
        );

        // Should be a job
//        $this->createThumb($photo);

        $Manager = $this
            ->managerRegistry
            ->getManagerForClass(get_class($entity));

        $Manager->persist($entity);
        $Manager->flush();

        return $entity;
    }

    public function getFilePath(Photo $photo)
    {
        return substr($this->appDir . '/files/' . chunk_split($photo->getReference(), 3, '/'), 0, -1);
    }

    public function getThumbPath(Photo $photo)
    {
        return $this->appDir . '/../web' . $this->getThumbUrl($photo);
    }

    public function getThumbUrl(Photo $photo)
    {
        return $this->getThumbUrlByReference($photo->getReference());
    }

    public function getThumbUrlByReference($reference)
    {
        return substr('/thumbs/' . chunk_split($reference, 3, '/'), 0, -1) . '.jpg';
    }

    public function getBase64Thumb($filepath, $height = 350, $width = 0)
    {
        $thumb = new \Imagick($filepath);
        $thumb->thumbnailImage($width, $height);

        return base64_encode($thumb->getImageBlob());
    }

    public function createThumb(Photo $photo, $height = 250, $width = 0)
    {
        $destination = $this->getThumbPath($photo);
        $source = $this->getFilePath($photo);

        $this
            ->filesystemService
            ->mkdir(dirname($destination));

        if (strstr($this->getMimeType($source), 'video')) {
            $video = FFMpeg::openFile($source);
            $source = $destination;

            $frame = $video->frame(Timecode::createFromSeconds(floor($video->format()->getDuration() / 2)));
            $frame->save($destination);
        }

        if (file_exists($source)) {
            $thumb = new \Imagick($source);
            $thumb->thumbnailImage($width, $height);
            $thumb->writeImage($destination);
        }

        return true;
    }

    public function transcoding(Photo $photo, $width = 0, $height = 1080)
    {
    }

    /**
     * @return string[]
     */
    public function getHashs()
    {
        $entityManager = $this
            ->managerRegistry
            ->getManagerForClass('JmoatiPhotosBundle:Hash');

        return array_column($entityManager
            ->getRepository('JmoatiPhotosBundle:Hash')
            ->createQueryBuilder('e')
            ->select('e.hash')
            ->getQuery()
            ->getScalarResult(), 'hash');
    }

    /**
     * @param string $hash
     * @return boolean
     */
    public function hashHash($hash)
    {
        $entityManager = $this
            ->managerRegistry
            ->getManagerForClass('JmoatiPhotosBundle:Hash');

        return (bool)$entityManager
            ->getRepository('JmoatiPhotosBundle:Hash')
            ->createQueryBuilder('e')
            ->select("count(1)")
            ->where('e.hash = :hash ')
            ->setParameter('hash', $hash)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getExifData(Photo $photo)
    {
        return $this->exifToolService->file($this->getFilePath($photo));
    }

    public function indexer(Photo $photo)
    {
        $thumbPath = $this->getThumbPath($photo);

        if (file_exists($thumbPath)){
            $thumb = base64_encode(file_get_contents($thumbPath));
        } else {
            $thumb = null;
        }

        $this->indexationService->updatePhoto($photo, $this->getExifData($photo), $thumb);
    }
}
