<?php

namespace Jmoati\PhotosBundle\Service;

use Elastica;
use Elastica\Type\Mapping;
use Elastica\Query;
use Elastica\QueryBuilder;
use Jmoati\PhotosBundle\Entity\Photo;
use Jmoati\PhotosBundle\Entity\Hash;

class IndexationService
{
    /**
     * @var Elastica\Client
     */
    protected $elasticaService;

    /**
     * @var string
     */
    protected $index;

    /**
     * @var PhotoService
     */
    protected $photoService;

    /**
     * @param string          $index
     * @param Elastica\Client $elasticaService
     */
    public function __construct($index, Elastica\Client $elasticaService)
    {
        $this->index           = $index;
        $this->elasticaService = $elasticaService;
    }

    public function setPhotoService(PhotoService $photoService)
    {
        $this->photoService = $photoService;
    }

    /**
     * @return Elastica\Index
     */
    public function getIndex()
    {
        $index = $this->elasticaService->getIndex($this->index);

        if (!$index->exists()) {
            $this->createIndex($index);
        }

        return $index;
    }

    /**
     * @param $name
     *
     * @return Elastica\Type
     */
    public function getType($name)
    {
        $type = $this->getIndex()->getType($name);

        if (!$type->exists()) {
            $this->createType($type);
        }

        return $type;

    }

    public function createIndex(Elastica\Index $index)
    {
        $mapping = array(
            'number_of_shards'   => 1,
            'number_of_replicas' => 1,
            'analysis'           => array(
                'analyzer' => array(
                    'defaultAnalyzer' => array(
                        'type'      => 'custom',
                        'tokenizer' => 'standard',
                        'filter'    => array('asciifolding', 'lowercase')
                    ),
                )
            )
        );

        $index->create($mapping);
    }

    public function createType(Elastica\Type $type)
    {
        $mapping = new Mapping($type);
        $mapping->setParam('index_analyzer', 'defaultAnalyzer');
        $mapping->setParam('search_analyzer', 'defaultAnalyzer');
        $mapping->setSource(array(
                                'excludes' => array("thumbnail"),
                            )
        );

        $schema = array(
            'mimetype'         => array(
                'type' => 'string',
            ),
            'exif'             => array(
                'type'    => 'object',
                'enabled' => false,
            ),
            'date'             => array(
                'type'   => 'date',
                'format' => 'yyyy-MM-dd HH:mm:ss',
            ),
            'filesize'         => array(
                "type" => "integer",
            ),
            'reference'        => array(
                'type' => 'string',
            ),
            'originalFilename' => array(
                'type' => 'string',
            ),
            'created'          => array(
                'type'   => 'date',
                'format' => 'yyyy-MM-dd HH:mm:ss',
            ),
            'thumbnail'        => array(
                "type"           => "image",
                "store"          => false,
                "include_in_all" => false,
                "feature"        => array(
                    "CEDD" => array(
                        "hash" => "BIT_SAMPLING",
                    ),
                )
            ),
        );

        $mapping->setProperties($schema);
        $mapping->send();
    }

    public function find()
    {
        $missing = new Elastica\Filter\Missing('date');

        $query = new Query\Filtered(new Query\MatchAll(), $missing);
        $query = new Query($query);

        $query->setSource(array('date', 'reference', 'thumb_url'));
        $query->setSize(10000);

        return $this->getIndex()->search($query);
    }

    /**
     * @param Photo  $photo
     * @param string $thumbnail
     */
    public function updatePhoto(Photo $photo, $exif, $thumbnail = null)
    {
        $date = null;
        foreach ($exif as $data) {
            if (isset($data['CreateDate'])) {
                try {
                    $date = new \DateTime($data['CreateDate']);
                    $date = $date->format('Y-m-d H:i:s');
                    break;
                } catch (\Exception $e) {
                }
            }
        }

        /** @var Hash $hash */
        $hash = $photo->getHashs()->first();
        $mimetype = $exif['File']['MIMEType'];

        $data = array(
            'filesize'         => $hash->getFilesize(),
            'reference'        => $photo->getReference(),
            'exif'             => $exif,
            'originalFilename' => $hash->getOriginalFilePath(),
            'created'          => $photo->getCreated()->format('Y-m-d H:i:s'),
            'mimetype'         => $mimetype,
            'date'             => $date,
            'thumb_url'        => $this->photoService->getThumbUrl($photo),
        );

        if (null !== $thumbnail) {
            $data['thumbnail'] = $thumbnail;
        }

        $document = new Elastica\Document();
        $document
            ->setId($photo->getId())
            ->setData($data)
            ->setDocAsUpsert(true);

        $this
            ->getType('photo')
            ->updateDocument($document);
    }

    protected function getSimilarQuery($base64Image)
    {
        $qb = new QueryBuilder\DSL\Query();

        $query = new Query($qb
                               ->bool()
                               ->addMust(new Query\Image(array(
                                                             'thumbnail' => array(
                                                                 'feature' => 'CEDD',
                                                                 'index'   => $this->getIndex()->getName(),
                                                                 'image'   => $base64Image,
                                                                 'hash'    => 'BIT_SAMPLING',
                                                                 'boost'   => 100,
                                                             )
                                                         )
                                         )
                               )
        );

        $query->setMinScore(175);

        return $query;
    }

    /**
     * @param string $base64Image
     *
     * @return Elastica\Result[]
     */
    public function findSimilar($base64Image)
    {
        $query = $this->getSimilarQuery($base64Image);
        $query->setSource(array('id'));

        return $this
            ->getIndex()
            ->search($query)
            ->getResults();
    }

    public function haveThumbnail($thumbnail)
    {
        return (bool) $this
            ->getIndex()
            ->search($this->getSimilarQuery($thumbnail))
            ->count();
    }
}