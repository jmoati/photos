<?php

namespace Jmoati\PhotosBundle\Command;

use Jmoati\FFMpeg\Data\Timecode;
use Jmoati\FFMpeg\FFMpeg;
use Jmoati\HelperBundle\Command\Command;
use Swarrot\Broker\Message;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CheckThumbCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('test')
            ->addOption('force', 'f', InputOption::VALUE_NONE);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $i = 0;
        $this
            ->get('doctrine')
            ->getManager()
            ->getConnection()
            ->getConfiguration()
            ->setSQLLogger(null);

        $result = $this
            ->get('doctrine.orm.default_entity_manager')
            ->getRepository('JmoatiPhotosBundle:Photo')
            ->createQueryBuilder('e')
            ->getQuery()
            ->getResult();

         /** @var \Jmoati\PhotosBundle\Entity\Photo $file */
        while ($file = array_shift($result)) {
            if (!file_exists($this->get('app.photos')->getFilePath($file))) {
                $this->get('doctrine')->getManager()->remove($file);
                $this->get('doctrine')->getManager()->flush();

                $output->writeln(++$i);
            }
        }

        $output->writeln("");
    }
}