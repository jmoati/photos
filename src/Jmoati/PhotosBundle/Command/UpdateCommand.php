<?php

namespace Jmoati\PhotosBundle\Command;

use Jmoati\FFMpeg\Data\Timecode;
use Jmoati\FFMpeg\FFMpeg;
use Jmoati\HelperBundle\Command\Command;
use Swarrot\Broker\Message;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateCommand extends Command
{
    protected function configure()
    {
        $this->setName('update');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $i = 0;
        $this
            ->get('doctrine')
            ->getManager()
            ->getConnection()
            ->getConfiguration()
            ->setSQLLogger(null);

        $result = $this
            ->get('doctrine.orm.default_entity_manager')
            ->getRepository('JmoatiPhotosBundle:Photo')
            ->createQueryBuilder('e')
            ->getQuery()
            ->getResult();

        $progress = new ProgressBar($output, count($result));
        $progress->setFormat('%current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s% %message%');
        $progress->start();

        /** @var \Jmoati\PhotosBundle\Entity\Photo $file */
        while ($file = array_shift($result)) {
            $progress->setMessage($file->getReference());
            $progress->advance();

            if ($file->getDate()){
                continue;
            }

            $exif = $this->get('jmoati.exiftool')->file($this->get('app.photos')->getFilePath($file));

            $date = null;
            foreach ($exif as $data) {
                if (isset($data['CreateDate'])) {
                    try {
                        $date = new \DateTime($data['CreateDate']);
                        $string = $date->format('Y-m-d H:i:s');

                        if (preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}\s[0-9]{2}:[0-9]{2}:[0-9]{2}$/', $string)) {
                            break;
                        } else {
                            $date = null;
                        }
                    } catch (\Exception $e) {
                        $date = null;
                    }
                }
            }

            $file
                ->setExif($exif)
                ->setDate($date)
                ->setMimetype($exif['File']['MIMEType']);

            if (++$i % 1000 == 0) {
                $this->get('doctrine')->getManager()->flush();
            }
        }


        $this->get('doctrine')->getManager()->flush();
        $progress->finish();
    }
}