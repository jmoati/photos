<?php

namespace Jmoati\PhotosBundle\Command;

use Jmoati\FFMpeg\Data\Timecode;
use Jmoati\FFMpeg\FFMpeg;
use Jmoati\HelperBundle\Command\Command;
use Swarrot\Broker\Message;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SetupCommand extends Command
{
    protected function configure()
    {
        $this->setName('setup');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

    }
}