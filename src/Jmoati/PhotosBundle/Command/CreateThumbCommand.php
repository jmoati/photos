<?php

namespace Jmoati\PhotosBundle\Command;

use Jmoati\FFMpeg\Data\Timecode;
use Jmoati\FFMpeg\FFMpeg;
use Jmoati\HelperBundle\Command\Command;
use Swarrot\Broker\Message;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateThumbCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('app:thumb:create')
            ->addOption('force', 'f', InputOption::VALUE_NONE);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this
            ->get('doctrine')
            ->getManager()
            ->getConnection()
            ->getConfiguration()
            ->setSQLLogger(null);

        $result = $this
            ->get('doctrine.orm.default_entity_manager')
            ->getRepository('JmoatiPhotosBundle:Photo')
            ->createQueryBuilder('e')
            ->getQuery()
            ->getResult();

        $this
            ->get('doctrine.orm.default_entity_manager')
            ->clear();

        $progress = new ProgressBar($output, count($result));
        $progress->setFormat('%current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s% %message%');
        $progress->start();

        while ($file = array_shift($result)) {
            $progress->setMessage($file->getReference());
            $progress->advance();

            $messagePublisher = $this->get('swarrot.publisher');
            $message = new Message($file->getId());

            $messagePublisher->publish('thumb_create', $message);

            $message = null;
        }

        $progress->finish();
        $output->writeln("");
    }
}