<?php

namespace Jmoati\PhotosBundle\Command;

use Jmoati\FFMpeg\Data\Timecode;
use Jmoati\FFMpeg\FFMpeg;
use Jmoati\HelperBundle\Command\Command;
use Swarrot\Broker\Message;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class IndexerCommand extends Command
{
    protected function configure()
    {
        $this->setName('app:indexer');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this
            ->get('doctrine')
            ->getManager()
            ->getConnection()
            ->getConfiguration()
            ->setSQLLogger(null);

        $result = $this
            ->get('doctrine.orm.default_entity_manager')
            ->getRepository('JmoatiPhotosBundle:Photo')
            ->createQueryBuilder('e')
            ->select('e.id', 'e.reference')
            ->getQuery()
            ->getResult();

        $this
            ->get('doctrine.orm.default_entity_manager')
            ->clear();

        $progress = new ProgressBar($output, count($result));
        $progress->setFormat('%current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s% %message%');
        $progress->start();

        while ($file = array_shift($result)) {
            $progress->setMessage($file['reference']);
            $progress->advance();

            $messagePublisher = $this->get('swarrot.publisher');
            $message = new Message($file['id']);

            $messagePublisher->publish('indexer', $message);
            $message = null;
        }

        $progress->finish();
        $output->writeln("");
    }
}